"use strict";
(self["webpackChunkreact_ts_default"] = self["webpackChunkreact_ts_default"] || []).push([[323],{

/***/ 8780:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ add_admin_form)
});

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(5861);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/slicedToArray.js + 3 modules
var slicedToArray = __webpack_require__(885);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(4687);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/formik/dist/formik.esm.js + 121 modules
var formik_esm = __webpack_require__(1054);
// EXTERNAL MODULE: ./node_modules/react-redux/es/index.js + 13 modules
var es = __webpack_require__(5998);
// EXTERNAL MODULE: ./src/api/listAdmin_superAdmin.ts
var listAdmin_superAdmin = __webpack_require__(7309);
// EXTERNAL MODULE: ./src/slices/dataAdmin_superAdmin/listData.tsx
var listData = __webpack_require__(4571);
;// CONCATENATED MODULE: ./src/components/form/add_admin/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"form":"RaLIrEmou2gEgYRr_Sws","groupField":"CMv7i65Q0STJG1o2uQG8","reset":"GUtHTGGrIBxN4pgl7t84","submit":"B3KUuycvT6kSoW83Hs1b"});
;// CONCATENATED MODULE: ./src/components/form/add_admin/form.tsx
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};






var FormComponent = function FormComponent(props) {
  var setModalOpen = props.setModalOpen;
  var dispatch = (0,es/* useDispatch */.I0)();
  var initialValues = {
    name: '',
    username: '',
    password: '',
    phone: '',
    usersCategory: 'admin',
    kodeDesa: undefined
  };
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  };
  var _useState = (0,react.useState)(''),
    _useState2 = (0,slicedToArray/* default */.Z)(_useState, 2),
    errorRes = _useState2[0],
    setErrorRes = _useState2[1];
  var handleSubmit = /*#__PURE__*/function () {
    var _ref = (0,asyncToGenerator/* default */.Z)( /*#__PURE__*/regenerator_default().mark(function _callee(formValue) {
      return regenerator_default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0,listAdmin_superAdmin/* addAdmin */.kA)(formValue);
            case 3:
              _context.t0 = _context.sent;
              if (!_context.t0) {
                _context.next = 6;
                break;
              }
              (0,listData/* fetchListDataAdmin */.ec)(tabelParams);
            case 6:
              setModalOpen(false);
              dispatch((0,listData/* fetchListDataAdmin */.ec)(tabelParams));
              _context.next = 13;
              break;
            case 10:
              _context.prev = 10;
              _context.t1 = _context["catch"](0);
              setErrorRes('Terjadi Kesalahan Teknis');
            case 13:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 10]]);
    }));
    return function handleSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  return /*#__PURE__*/react.createElement("div", null, /*#__PURE__*/react.createElement(formik_esm/* Formik */.J9, {
    initialValues: initialValues,
    onSubmit: function onSubmit(value) {
      handleSubmit(value);
    }
  }, function (_ref2) {
    var values = _ref2.values;
    var disabled = Boolean(values.name && values.username && values.phone && values.password);
    return /*#__PURE__*/react.createElement(formik_esm/* Form */.l0, null, /*#__PURE__*/react.createElement("div", {
      className: style_module.form
    }, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "nama"
    }, "Nama"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "name",
      id: "nama",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Nama"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "username"
    }, "Username"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "username",
      id: "username",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Username "
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "password"
    }, "Password"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "password",
      id: "password",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Password"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "phone"
    }, "Telepon"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "phone",
      id: "phone",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Telephone"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "kodeDesa"
    }, "Kode Wilayah"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "kodeDesa",
      id: "kodeDesa",
      type: "number",
      autoComplete: "off",
      placeholder: "Masukan Kode Wilayah"
    }), /*#__PURE__*/react.createElement("span", null))), errorRes ? /*#__PURE__*/react.createElement("p", {
      className: "flex justify-center pt-3 text-sm text-red"
    }, "Terjadi Kesalahan Teknis") : '', /*#__PURE__*/react.createElement("div", {
      className: "flex justify-between items-center space-x-2 pt-4"
    }, /*#__PURE__*/react.createElement("button", {
      type: "submit",
      className: style_module.submit,
      disabled: !disabled
    }, "Submit")));
  }));
};
__signature__(FormComponent, "useDispatch{dispatch}\nuseState{[errorRes, setErrorRes]('')}", function () {
  return [es/* useDispatch */.I0];
});
var _default = FormComponent;
/* harmony default export */ const add_admin_form = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(FormComponent, "FormComponent", "D:\\Project\\Base_Code\\src\\components\\form\\add_admin\\form.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\form\\add_admin\\form.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 2550:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ edit_Admin_form)
});

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(5861);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/slicedToArray.js + 3 modules
var slicedToArray = __webpack_require__(885);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(4687);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/formik/dist/formik.esm.js + 121 modules
var formik_esm = __webpack_require__(1054);
// EXTERNAL MODULE: ./node_modules/react-redux/es/index.js + 13 modules
var es = __webpack_require__(5998);
;// CONCATENATED MODULE: ./src/components/form/edit_Admin/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"form":"JWH9cnXaVl_CwALKBoID","groupField":"eYF040WbD0HhnzLh9NlC","reset":"Nzn0U9aopOto_ZaMWzNH","submit":"C4EhLlbrgVcje4nJwI86"});
// EXTERNAL MODULE: ./src/api/listAdmin_superAdmin.ts
var listAdmin_superAdmin = __webpack_require__(7309);
// EXTERNAL MODULE: ./src/slices/dataAdmin_superAdmin/listData.tsx
var listData = __webpack_require__(4571);
;// CONCATENATED MODULE: ./src/components/form/edit_Admin/form.tsx
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};






var FormComponent = function FormComponent(props) {
  var initialData = props.initialData,
    setModalOpen = props.setModalOpen;
  var dispatch = (0,es/* useDispatch */.I0)();
  var initialValues = {
    name: initialData.name,
    phone: initialData.noHp,
    username: initialData.name,
    password: '',
    kodeDesa: initialData.kodeDesa || '',
    usersCategory: 'admin'
  };
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  };
  var _useState = (0,react.useState)(''),
    _useState2 = (0,slicedToArray/* default */.Z)(_useState, 2),
    errorRes = _useState2[0],
    setErrosRes = _useState2[1];
  var handleSubmit = /*#__PURE__*/function () {
    var _ref = (0,asyncToGenerator/* default */.Z)( /*#__PURE__*/regenerator_default().mark(function _callee(formValue) {
      return regenerator_default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0,listAdmin_superAdmin/* editAdmin */.iK)(initialData.id, formValue);
            case 3:
              dispatch((0,listData/* fetchListDataAdmin */.ec)(tabelParams));
              setModalOpen(false);
              _context.next = 10;
              break;
            case 7:
              _context.prev = 7;
              _context.t0 = _context["catch"](0);
              setErrosRes('Terjadi Kesalahan Teknis');
            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 7]]);
    }));
    return function handleSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  return /*#__PURE__*/react.createElement("div", null, /*#__PURE__*/react.createElement(formik_esm/* Formik */.J9, {
    initialValues: initialValues,
    onSubmit: function onSubmit(value) {
      handleSubmit(value);
    }
  }, function (_ref2) {
    var values = _ref2.values;
    var disabled = Boolean(values.name && values.username && values.phone && values.kodeDesa);
    return /*#__PURE__*/react.createElement(formik_esm/* Form */.l0, null, /*#__PURE__*/react.createElement("div", {
      className: style_module.form
    }, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "nama"
    }, "Nama"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "name",
      id: "nama",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Nama"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "phone"
    }, "Telepon"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "phone",
      id: "phone",
      type: "phone",
      autoComplete: "off",
      placeholder: "Masukan Telepone"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "username"
    }, "UserName"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "username",
      id: "username",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Username "
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "password"
    }, "Password"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "password",
      id: "password",
      type: "text",
      autoComplete: "off",
      placeholder: "************"
    }), /*#__PURE__*/react.createElement("span", null)), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "kodeDesa"
    }, "Kode Wilayah"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "kodeDesa",
      id: "kodeDesa",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Kode Wilayah"
    }), /*#__PURE__*/react.createElement("span", null))), errorRes ? /*#__PURE__*/react.createElement("p", {
      className: "flex justify-center pt-3 text-sm text-red"
    }, "Terjadi Kesalahan Teknis") : '', /*#__PURE__*/react.createElement("div", {
      className: "flex justify-between items-center space-x-2 pt-4"
    }, /*#__PURE__*/react.createElement("button", {
      type: "submit",
      className: style_module.submit,
      disabled: !disabled
    }, "Submit")));
  }));
};
__signature__(FormComponent, "useDispatch{dispatch}\nuseState{[errorRes, setErrosRes]('')}", function () {
  return [es/* useDispatch */.I0];
});
var _default = FormComponent;
/* harmony default export */ const edit_Admin_form = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(FormComponent, "FormComponent", "D:\\Project\\Base_Code\\src\\components\\form\\edit_Admin\\form.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\form\\edit_Admin\\form.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 2866:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "m": () => (/* binding */ AddAdminModal)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _form_add_admin_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8780);
/* harmony import */ var _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9928);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var AddAdminModal = function AddAdminModal(props) {
  var modalOpen = props.modalOpen,
    setModalOpen = props.setModalOpen;
  var customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex: '10',
      overflow: 'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline: 'none',
      border: 'none',
      borderRadius: '10px'
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_1___default()), {
    isOpen: modalOpen,
    style: customStyle,
    ariaHideApp: false
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__,
    alt: "ic-close",
    onClick: function onClick() {
      return setModalOpen(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center text-center mb-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "font-semibold text-xl text-[#091D60]"
  }, "Tambah Admin"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_form_add_admin_form__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
    setModalOpen: setModalOpen
    // initialData={initialData}
  })));
};


;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(AddAdminModal, "AddAdminModal", "D:\\Project\\Base_Code\\src\\components\\popUp\\addAdmin.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 4019:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9928);
/* harmony import */ var _assets_icons_ic_delete_user_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9371);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var DeletePopUp = function DeletePopUp(props) {
  var funcDelete = props.funcDelete,
    modalOpen = props.modalOpen,
    setModalOpen = props.setModalOpen,
    selectedId = props.selectedId,
    selectedName = props.selectedName,
    errorRes = props.errorRes,
    setErrorRes = props.setErrorRes;
  var customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex: '10',
      overflow: 'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline: 'none',
      border: 'none',
      borderRadius: '10px',
      paddingBottom: '25px'
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_1___default()), {
    ariaHideApp: false,
    isOpen: modalOpen,
    style: customStyle
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80 h-72 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end h-fit"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_2__,
    alt: "ic-close",
    onClick: function onClick() {
      setErrorRes('');
      setModalOpen(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "grid justify-items-center text-white space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "font-semibold text-2xl text-[#091D60]"
  }, "Hapus Data"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "grid place-items-center pt-3 space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    className: "animate-pulse",
    src: _assets_icons_ic_delete_user_png__WEBPACK_IMPORTED_MODULE_3__,
    alt: "ic-delete-user"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    className: "font-semibold text-xl text-[#6D6D6D]"
  }, selectedName))), errorRes ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    className: "text-sm text-red"
  }, "Failed to delete data") : '', /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-7 space-x-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "w-28 h-9 bg-secondary-1 text-white rounded-10",
    onClick: function onClick() {
      setErrorRes('');
      setModalOpen(false);
    }
  }, "Batal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "w-28 h-9 bg-red text-white rounded-10",
    onClick: function onClick() {
      funcDelete(selectedId);
    }
  }, "Hapus"))));
};
var _default = DeletePopUp;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(DeletePopUp, "DeletePopUp", "D:\\Project\\Base_Code\\src\\components\\popUp\\deleteAlert.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\popUp\\deleteAlert.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 1237:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "x": () => (/* binding */ EditAdminModal)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _form_edit_Admin_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2550);
/* harmony import */ var _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9928);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




// import { EditAttendanceApiParams } from '../../tools/types';

var EditAdminModal = function EditAdminModal(props) {
  var modalOpen = props.modalOpen,
    setModalOpen = props.setModalOpen,
    initialData = props.initialData;
  var customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex: '10',
      overflow: 'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline: 'none',
      border: 'none',
      borderRadius: '10px'
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_1___default()), {
    isOpen: modalOpen,
    style: customStyle,
    ariaHideApp: false
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__,
    alt: "ic-close",
    onClick: function onClick() {
      return setModalOpen(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "font-semibold text-xl text-[#091D60]"
  }, "Edit Admin"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_form_edit_Admin_form__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
    initialData: initialData,
    setModalOpen: setModalOpen
  })));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(EditAdminModal, "EditAdminModal", "D:\\Project\\Base_Code\\src\\components\\popUp\\editAdmin.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8009:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4849);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var defaultThemeProps = {
  children: ''
};
var Theme = function Theme(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__/* .SkeletonTheme */ .y, {
    baseColor: "#5C6673",
    highlightColor: "#B6B6B6"
  }, children);
};
Theme.defaultProps = defaultThemeProps;
var _default = Theme;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultThemeProps, "defaultThemeProps", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(Theme, "Theme", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8330:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Theme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8009);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _Theme__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3069:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "k": () => (/* binding */ ListData)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6055);
/* harmony import */ var _components_popUp_addAdmin__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2866);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9030);
/* harmony import */ var _slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4571);
/* harmony import */ var _assets_icons_ic_search_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5449);
/* harmony import */ var _assets_icons_ic_addUser_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2502);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};







var ListData = function ListData() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppSelector */ .C)(function (state) {
      return state.listAdminSuperAdmin;
    }),
    data = _useAppSelector.data,
    meta = _useAppSelector.meta;
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z)(_useState, 2),
    searchValue = _useState2[0],
    setSearchValue = _useState2[1];
  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
    _useState4 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z)(_useState3, 2),
    addAdminModal = _useState4[0],
    setAddAdminModal = _useState4[1];
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: searchValue
  };
  var onChangePage = function onChangePage(page) {
    dispatch((0,_slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_4__/* .setLoadMore */ .Uq)(true));
    dispatch((0,_slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_4__/* .fetchListDataAdmin */ .ec)({
      page: page,
      size: tabelParams.size,
      order: '',
      sort: '',
      search: searchValue
    }));
  };
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_4__/* .fetchListDataAdmin */ .ec)(tabelParams));
  }, [searchValue]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-between items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Data Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex space-x-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center items-center p-2 bg-[#DEE9FF] rounded-lg space-x-3 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: _assets_icons_ic_search_svg__WEBPACK_IMPORTED_MODULE_5__,
    alt: "search"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    className: "outline-none text-black bg-transparent",
    placeholder: "Cari Nama",
    onChange: function onChange(e) {
      return setSearchValue(e.target.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center items-center p-2 bg-[#0957F9] rounded-lg space-x-3 cursor-pointer",
    role: "presentation",
    onClick: function onClick() {
      return setAddAdminModal(true);
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: _assets_icons_ic_addUser_svg__WEBPACK_IMPORTED_MODULE_6__,
    alt: "search"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", null, "Tambah")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_table__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
    data: data,
    loading: false,
    meta: meta,
    onChangePage: onChangePage,
    paginationPerPage: tabelParams.size
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_popUp_addAdmin__WEBPACK_IMPORTED_MODULE_2__/* .AddAdminModal */ .m, {
    modalOpen: addAdminModal,
    setModalOpen: setAddAdminModal
  }));
};
__signature__(ListData, "useAppDispatch{dispatch}\nuseAppSelector{{ data, meta }}\nuseState{[ searchValue, setSearchValue ]('')}\nuseState{[addAdminModal , setAddAdminModal](false)}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(ListData, "ListData", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\listData.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9649:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "W": () => (/* binding */ CellSkeleton)
});

// UNUSED EXPORTS: default

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/react-loading-skeleton/dist/index.mjs
var dist = __webpack_require__(4849);
// EXTERNAL MODULE: ./src/components/skeleton/index.tsx
var skeleton = __webpack_require__(8330);
;// CONCATENATED MODULE: ./src/features/dataAdmin_superAdmin/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"summaryCard":"ra4ykAJftOG25QMW5WWX","wrapperCard":"_eccY3M0qsL5RfyI3kbg","wrapperGrafik":"z3vb5lsLnNxMuJ49DygZ","table":"vbwj0FWAP7SaAxlvMndN","header":"QFCo3K0LMN0R0ENdhWdr","row":"OjKgNzjMhr2od7cP26kI"});
;// CONCATENATED MODULE: ./src/features/dataAdmin_superAdmin/skeleton.tsx
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var defaultCellProps = {
  children: ''
};
var CellSkeleton = function CellSkeleton(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react.createElement(skeleton/* default */.Z, null, /*#__PURE__*/react.createElement("span", {
    className: "relative"
  }, children, /*#__PURE__*/react.createElement("div", {
    className: "absolute top-0 right-0 bottom-0 left-0"
  }, /*#__PURE__*/react.createElement(dist/* default */.Z, {
    height: "100%",
    width: "100%"
  }))));
};
CellSkeleton.defaultProps = defaultCellProps;
var defaultTableProps = {
  numbering: true,
  header: true,
  column: 5,
  row: 5
};
var TableSkeleton = function TableSkeleton(_ref2) {
  var header = _ref2.header,
    numbering = _ref2.numbering,
    column = _ref2.column,
    row = _ref2.row;
  var gridTemplateColumns = '';
  var xAxis = [];
  var yAxis = [];
  if (numbering) {
    gridTemplateColumns = '5em ';
    xAxis.push(-1);
  }
  if (column) {
    for (var i = 0; i < column; i++) {
      xAxis.push(i);
      gridTemplateColumns += '1fr ';
    }
  }
  if (row) {
    for (var _i = 0; _i < row; _i++) {
      yAxis.push(_i);
    }
  }
  return /*#__PURE__*/react.createElement(skeleton/* default */.Z, null, header && /*#__PURE__*/react.createElement("div", {
    className: style_module.header,
    style: {
      gridTemplateColumns: gridTemplateColumns
    }
  }, column && xAxis.map(function (x) {
    return /*#__PURE__*/react.createElement(dist/* default */.Z, {
      height: "1em",
      key: x
    });
  })), yAxis.length && yAxis.map(function (y) {
    return /*#__PURE__*/react.createElement("div", {
      className: style_module.row,
      style: {
        gridTemplateColumns: gridTemplateColumns
      },
      key: y
    }, xAxis.length && xAxis.map(function (x) {
      return /*#__PURE__*/react.createElement(dist/* default */.Z, {
        height: "1em",
        key: x
      });
    }));
  }));
};
TableSkeleton.defaultProps = defaultTableProps;
var _default = TableSkeleton;
/* harmony default export */ const dataAdmin_superAdmin_skeleton = ((/* unused pure expression or super */ null && (_default)));
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultCellProps, "defaultCellProps", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\skeleton.tsx");
  reactHotLoader.register(CellSkeleton, "CellSkeleton", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\skeleton.tsx");
  reactHotLoader.register(defaultTableProps, "defaultTableProps", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\skeleton.tsx");
  reactHotLoader.register(TableSkeleton, "TableSkeleton", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\skeleton.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\skeleton.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 6055:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(5861);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(885);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4687);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7294);
/* harmony import */ var react_data_table_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(44);
/* harmony import */ var _skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9649);
/* harmony import */ var _components_popUp_editAdmin__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1237);
/* harmony import */ var _assets_icons_edit_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(839);
/* harmony import */ var _assets_icons_ic_delete_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(8079);
/* harmony import */ var _components_popUp_deleteAlert__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4019);
/* harmony import */ var _api_listAdmin_superAdmin__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(7309);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9030);
/* harmony import */ var _slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4571);
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};










var defaultProps = {
  loading: false,
  paginationPerPage: 10
};
var TableComponent = function TableComponent(_ref) {
  var data = _ref.data,
    loading = _ref.loading,
    paginationPerPage = _ref.paginationPerPage,
    meta = _ref.meta,
    onChangePage = _ref.onChangePage;
  var loadingCell = (0,react__WEBPACK_IMPORTED_MODULE_1__.useCallback)(function (cell) {
    return loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_skeleton__WEBPACK_IMPORTED_MODULE_3__/* .CellSkeleton */ .W, null, cell) : cell;
  }, [loading]);
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_9__/* .useAppDispatch */ .T)();
  var _React$useState = react__WEBPACK_IMPORTED_MODULE_1__.useState(false),
    _React$useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z)(_React$useState, 2),
    modalEdit = _React$useState2[0],
    setModalEdit = _React$useState2[1];
  var _React$useState3 = react__WEBPACK_IMPORTED_MODULE_1__.useState(false),
    _React$useState4 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z)(_React$useState3, 2),
    modalDelete = _React$useState4[0],
    setModalDelete = _React$useState4[1];
  var _React$useState5 = react__WEBPACK_IMPORTED_MODULE_1__.useState({}),
    _React$useState6 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z)(_React$useState5, 2),
    initialData = _React$useState6[0],
    setInitialdata = _React$useState6[1];
  var _React$useState7 = react__WEBPACK_IMPORTED_MODULE_1__.useState(''),
    _React$useState8 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_11__/* ["default"] */ .Z)(_React$useState7, 2),
    errorRes = _React$useState8[0],
    setErrorRes = _React$useState8[1];
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  };
  var handleDelete = /*#__PURE__*/function () {
    var _ref2 = (0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_12__/* ["default"] */ .Z)( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(id) {
      var response;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0,_api_listAdmin_superAdmin__WEBPACK_IMPORTED_MODULE_8__/* .deleteAdmin */ .jP)(id);
            case 3:
              response = _context.sent;
              if (!response.success) {
                setModalDelete(true);
                setErrorRes(response.message);
              } else {
                setModalDelete(false);
                dispatch((0,_slices_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_10__/* .fetchListDataAdmin */ .ec)(tabelParams));
              }
              _context.next = 9;
              break;
            case 7:
              _context.prev = 7;
              _context.t0 = _context["catch"](0);
            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 7]]);
    }));
    return function handleDelete(_x) {
      return _ref2.apply(this, arguments);
    };
  }();
  var columns = [{
    name: 'No.',
    cell: function cell(row) {
      return loadingCell(row.no);
    },
    width: '5rem'
  }, {
    name: 'Nama',
    cell: function cell(row) {
      return loadingCell(row.name);
    }
  }, {
    name: 'Telephone',
    cell: function cell(row) {
      return loadingCell(row.phone);
    }
  }, {
    name: 'Kategori Admin',
    cell: function cell(row) {
      return loadingCell(row.category);
    }
  }, {
    name: 'Aksi',
    cell: function cell(row) {
      return loadingCell( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
        className: "flex space-x-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
        className: "cursor-pointer",
        role: "presentation",
        onClick: function onClick() {
          setInitialdata(row);
          setModalEdit(true);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("img", {
        src: _assets_icons_edit_svg__WEBPACK_IMPORTED_MODULE_5__,
        alt: "eye"
      })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
        className: "cursor-pointer",
        role: "presentation",
        onClick: function onClick() {
          setInitialdata(row);
          setModalDelete(true);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("img", {
        src: _assets_icons_ic_delete_svg__WEBPACK_IMPORTED_MODULE_6__,
        alt: "eye"
      }))));
    },
    width: '5rem'
  }];
  var customStyles = {
    table: {
      style: {
        backgroundColor: 'none'
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);'
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react_data_table_component__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .ZP, {
    columns: columns,
    data: data,
    customStyles: customStyles,
    pagination: true,
    paginationServer: true,
    paginationPerPage: paginationPerPage,
    paginationDefaultPage: meta === null || meta === void 0 ? void 0 : meta.page,
    paginationTotalRows: meta === null || meta === void 0 ? void 0 : meta.totalData,
    onChangePage: onChangePage,
    paginationComponentOptions: {
      noRowsPerPage: true,
      rangeSeparatorText: 'dari'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_components_popUp_editAdmin__WEBPACK_IMPORTED_MODULE_4__/* .EditAdminModal */ .x, {
    initialData: initialData,
    modalOpen: modalEdit,
    setModalOpen: setModalEdit
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_components_popUp_deleteAlert__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
    modalOpen: modalDelete,
    setModalOpen: setModalDelete,
    selectedName: initialData.name,
    selectedId: initialData.id,
    funcDelete: handleDelete,
    errorRes: errorRes,
    setErrorRes: setErrorRes
  }));
};
__signature__(TableComponent, "useCallback{loadingCell}\nuseAppDispatch{dispatch}\nuseState{[ modalEdit, setModalEdit ](false)}\nuseState{[ modalDelete, setModalDelete ](false)}\nuseState{[ initialData, setInitialdata]({})}\nuseState{[ errorRes , setErrorRes ]('')}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_9__/* .useAppDispatch */ .T];
});
TableComponent.defaultProps = defaultProps;
var _default = TableComponent;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultProps, "defaultProps", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\table.tsx");
  reactHotLoader.register(TableComponent, "TableComponent", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\table.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\dataAdmin_superAdmin\\table.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 5006:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ DataAdmin)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _features_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3069);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var DataAdmin = function DataAdmin() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_dataAdmin_superAdmin_listData__WEBPACK_IMPORTED_MODULE_1__/* .ListData */ .k, null);
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(DataAdmin, "DataAdmin", "D:\\Project\\Base_Code\\src\\pages\\dataAdmin_superAdmin\\dataAdmin.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8323:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _dataAdmin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5006);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _dataAdmin__WEBPACK_IMPORTED_MODULE_0__/* .DataAdmin */ .Z;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\pages\\dataAdmin_superAdmin\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9928:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/36f57921ad40b2051363.svg";

/***/ }),

/***/ 839:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/a5c36060e8087cff009e.svg";

/***/ }),

/***/ 2502:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/1158e7f086797332bd1c.svg";

/***/ }),

/***/ 9371:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/9ed705f36d37bf65349e.png";

/***/ }),

/***/ 8079:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/2380b7520106a60f06d2.svg";

/***/ }),

/***/ 5449:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/b4ecf84f6fabb207bf70.svg";

/***/ })

}]);
//# sourceMappingURL=323.3ba9efe2ed0cdfa69c38.bundle.js.map
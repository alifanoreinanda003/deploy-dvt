"use strict";
(self["webpackChunkreact_ts_default"] = self["webpackChunkreact_ts_default"] || []).push([[279],{

/***/ 4704:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "L": () => (/* binding */ editConfidence)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5861);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4687);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_httpRequest__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7786);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var SERVER_HOST = "https://servers.digivoote.com/dvt";
var editConfidence = /*#__PURE__*/function () {
  var _ref = (0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(id, params) {
    var header, response;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            header = {
              Authorization: "Bearer ".concat(localStorage.getItem('token_dvt'))
            };
            _context.next = 3;
            return _tools_httpRequest__WEBPACK_IMPORTED_MODULE_1__/* .edit */ .eP("".concat(SERVER_HOST, "/admin/data/insert/confidence/").concat(id), params, header);
          case 3:
            response = _context.sent;
            return _context.abrupt("return", response);
          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return function editConfidence(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(SERVER_HOST, "SERVER_HOST", "D:\\Project\\Base_Code\\src\\api\\detailData.ts");
  reactHotLoader.register(editConfidence, "editConfidence", "D:\\Project\\Base_Code\\src\\api\\detailData.ts");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 5088:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "p": () => (/* binding */ getTimList)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5861);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4687);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_httpRequest__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7786);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var SERVER_HOST = "https://servers.digivoote.com/dvt";
var getTimList = /*#__PURE__*/function () {
  var _ref = (0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z)( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
    var header, response;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            header = {
              Authorization: "Bearer ".concat(localStorage.getItem('token_dvt'))
            };
            _context.next = 3;
            return _tools_httpRequest__WEBPACK_IMPORTED_MODULE_1__/* .get */ .U2("".concat(SERVER_HOST, "/tim/list?used=list"), undefined, header);
          case 3:
            response = _context.sent;
            return _context.abrupt("return", response);
          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return function getTimList() {
    return _ref.apply(this, arguments);
  };
}();

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(SERVER_HOST, "SERVER_HOST", "D:\\Project\\Base_Code\\src\\api\\getListTim.ts");
  reactHotLoader.register(getTimList, "getTimList", "D:\\Project\\Base_Code\\src\\api\\getListTim.ts");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 7607:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4942);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7294);
/* harmony import */ var formik__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1054);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1951);
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0,_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z)(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};



var defaultProps = {
  defaultDetail: undefined
};
var SelectionField = function SelectionField(props) {
  var name = props.name,
    placeholder = props.placeholder,
    selectOptions = props.selectOptions,
    defaultDetail = props.defaultDetail;
  var _useField = (0,formik__WEBPACK_IMPORTED_MODULE_2__/* .useField */ .U$)(name),
    _useField2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)(_useField, 1),
    field = _useField2[0];
  var selectStyle = {
    control: function control(base) {
      return _objectSpread(_objectSpread({}, base), {}, {
        background: 'rgba(9,29,96,0.1)',
        height: '3rem',
        fontSize: '14px',
        borderRadius: '10px',
        fontWeight: '500',
        outline: 'none',
        border: 'none',
        paddingBottom: '0.6rem',
        colors: 'black'
      });
    },
    placeholder: function placeholder(base) {
      return _objectSpread(_objectSpread({}, base), {}, {
        color: '#091D60',
        marginBottom: '0.5rem'
      });
    },
    option: function option(base, state) {
      return _objectSpread(_objectSpread({}, base), {}, {
        color: "black",
        fontWeight: '500',
        fontSize: '14px',
        overflow: 'hidden'
      });
    },
    dropdownIndicator: function dropdownIndicator(base) {
      return _objectSpread(_objectSpread({}, base), {}, {
        marginBottom: '0.5rem'
      });
    },
    valueContainer: function valueContainer(provided) {
      return _objectSpread(_objectSpread({}, provided), {}, {
        minHeight: "auto",
        flexWrap: "nowrap",
        overflowX: "auto",
        padding: "0 .5rem",
        color: 'white'
      });
    },
    input: function input(base) {
      return _objectSpread(_objectSpread({}, base), {}, {
        color: "white"
      });
    },
    singleValue: function singleValue(base) {
      return _objectSpread(_objectSpread({}, base), {}, {
        color: 'black',
        marginBottom: '0.5rem'
      });
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react_select__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .ZP, {
    id: "company",
    options: selectOptions,
    styles: selectStyle,
    placeholder: placeholder,
    defaultValue: {
      label: defaultDetail,
      value: defaultDetail
    },
    components: {
      IndicatorSeparator: function IndicatorSeparator() {
        return null;
      }
    },
    onChange: function onChange(val) {
      return field.onChange({
        target: {
          value: val === null || val === void 0 ? void 0 : val.value,
          name: name
        }
      });
    }
  }));
};
__signature__(SelectionField, "useField{[ field ]}", function () {
  return [formik__WEBPACK_IMPORTED_MODULE_2__/* .useField */ .U$];
});
SelectionField.defaultProps = defaultProps;
var _default = SelectionField;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultProps, "defaultProps", "D:\\Project\\Base_Code\\src\\components\\field\\selectionField.tsx");
  reactHotLoader.register(SelectionField, "SelectionField", "D:\\Project\\Base_Code\\src\\components\\field\\selectionField.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\field\\selectionField.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8024:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ attendance_edit_form)
});

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/formik/dist/formik.esm.js + 121 modules
var formik_esm = __webpack_require__(1054);
// EXTERNAL MODULE: ./src/components/field/selectionField.tsx
var selectionField = __webpack_require__(7607);
;// CONCATENATED MODULE: ./src/components/form/attendance_edit/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"form":"QdyYn6kauJ3mwZ0Oucog","groupField":"c7s8EcQoQnEcGDm9O5Kg"});
// EXTERNAL MODULE: ./src/api/detailData.ts
var detailData = __webpack_require__(4704);
;// CONCATENATED MODULE: ./src/components/form/attendance_edit/form.tsx
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


// import { EditAttendanceApiParams } from '../../../tools/types';



var FromAddComponent = function FromAddComponent(props) {
  var initialValues = {
    Canvasing: 0,
    Kedatangan: 0,
    Penyebaran: 0
  };
  var canvasPoint = [{
    value: 1,
    label: '1 Point'
  }, {
    value: 2,
    label: '2 Point'
  }, {
    value: 3,
    label: '3 Point'
  }];
  var KedatanganPoint = [{
    value: 1,
    label: '1 Point'
  }, {
    value: 2,
    label: '2 Point'
  }];
  var PersebaranAPKPoint = [{
    value: 1,
    label: '1 Point'
  }, {
    value: 2,
    label: '2 Point'
  }];
  return /*#__PURE__*/react.createElement(formik_esm/* Formik */.J9, {
    initialValues: initialValues,
    onSubmit: function onSubmit(values) {
      var confidenceFiltered = {
        canvasing: values.Canvasing,
        calon: values.Kedatangan,
        apk: values.Penyebaran
      };
      (0,detailData/* editConfidence */.L)(props.selectedNIK, confidenceFiltered);
      props.setModalOpen(false);
      // window.location.reload();
    }
  }, function (_ref) {
    var values = _ref.values;
    var TotalAmount = (values.Canvasing + values.Kedatangan + values.Penyebaran) * 10;
    return /*#__PURE__*/react.createElement(formik_esm/* Form */.l0, null, /*#__PURE__*/react.createElement("div", {
      className: style_module.form
    }, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "Canvasing"
    }, "Canvashing"), /*#__PURE__*/react.createElement("div", {
      className: "w-28"
    }, /*#__PURE__*/react.createElement(selectionField/* default */.Z, {
      name: "Canvasing",
      placeholder: "Masukan Point",
      selectOptions: canvasPoint
    }))), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "Kedatangan"
    }, "Kedatangan Calon"), /*#__PURE__*/react.createElement("div", {
      className: "w-28"
    }, /*#__PURE__*/react.createElement(selectionField/* default */.Z, {
      name: "Kedatangan",
      placeholder: "Masukan Point",
      selectOptions: KedatanganPoint
    }))), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "Penyebaran"
    }, "Penyebaran APK"), /*#__PURE__*/react.createElement("div", {
      className: "w-28"
    }, /*#__PURE__*/react.createElement(selectionField/* default */.Z, {
      name: "Penyebaran",
      placeholder: "Masukan Point",
      selectOptions: PersebaranAPKPoint
    }))), /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "Total"
    }, "Total"), /*#__PURE__*/react.createElement("div", {
      className: "w-28 text-[#091D60] font-semibold"
    }, TotalAmount, "%")), /*#__PURE__*/react.createElement("button", {
      className: "w-full px-4 py-3 font-medium rounded-lg bg-[#1877F2] text-white",
      type: "submit"
    }, "Kirim")));
  });
};
var _default = FromAddComponent;
/* harmony default export */ const attendance_edit_form = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(FromAddComponent, "FromAddComponent", "D:\\Project\\Base_Code\\src\\components\\form\\attendance_edit\\form.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\form\\attendance_edit\\form.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 231:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "j": () => (/* binding */ EditAttendanceModal)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _form_attendance_edit_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8024);
/* harmony import */ var _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9928);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var EditAttendanceModal = function EditAttendanceModal(props) {
  var modalOpen = props.modalOpen,
    setModalOpen = props.setModalOpen,
    selectedNIK = props.selectedNIK,
    selectedName = props.selectedName;
  var customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.30)',
      zIndex: '10',
      overflow: 'visible'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline: 'none',
      border: 'none',
      borderRadius: '10px'
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_1___default()), {
    isOpen: modalOpen,
    style: customStyle,
    ariaHideApp: false
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_close_svg__WEBPACK_IMPORTED_MODULE_3__,
    alt: "ic-close",
    onClick: function onClick() {
      return setModalOpen(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "font-semibold text-xl text-[#091D60]"
  }, selectedName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    className: "font-semibold text-base text-[#6D6D6D]"
  }, selectedNIK))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_form_attendance_edit_form__WEBPACK_IMPORTED_MODULE_2__/* ["default"] */ .Z, {
    selectedNIK: selectedNIK,
    setModalOpen: setModalOpen
  })));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(EditAttendanceModal, "EditAttendanceModal", "D:\\Project\\Base_Code\\src\\components\\popUp\\editAttendance.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 1943:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5861);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(885);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4687);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7294);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1951);
/* harmony import */ var _api_getListTim__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5088);
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};



var SelectTim = function SelectTim(props) {
  var _onChange = props.onChange;
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z)(_useState, 2),
    options = _useState2[0],
    setOptions = _useState2[1];
  var fetchListTim = /*#__PURE__*/function () {
    var _ref = (0,_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4__/* ["default"] */ .Z)( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
      var _response$data, response, mapOptions;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0,_api_getListTim__WEBPACK_IMPORTED_MODULE_2__/* .getTimList */ .p)();
            case 3:
              response = _context.sent;
              mapOptions = response && (response === null || response === void 0 ? void 0 : (_response$data = response.data) === null || _response$data === void 0 ? void 0 : _response$data.map(function (item) {
                return {
                  value: item.id,
                  label: item.name
                };
              }));
              setOptions(mapOptions);
              _context.next = 10;
              break;
            case 8:
              _context.prev = 8;
              _context.t0 = _context["catch"](0);
            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 8]]);
    }));
    return function fetchListTim() {
      return _ref.apply(this, arguments);
    };
  }();
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(function () {
    fetchListTim();
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react_select__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .ZP, {
    isClearable: true,
    id: "company",
    className: "text-black capitalize",
    options: options,
    placeholder: "Pilih nama Tim",
    components: {
      IndicatorSeparator: function IndicatorSeparator() {
        return null;
      }
    },
    onChange: function onChange(val) {
      return _onChange((val === null || val === void 0 ? void 0 : val.value) || '');
    }
  }));
};
__signature__(SelectTim, "useState{[options, setOptions]}\nuseEffect{}");
var _default = SelectTim;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(SelectTim, "SelectTim", "D:\\Project\\Base_Code\\src\\components\\select\\select-tim.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\select\\select-tim.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8009:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4849);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var defaultThemeProps = {
  children: ''
};
var Theme = function Theme(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__/* .SkeletonTheme */ .y, {
    baseColor: "#5C6673",
    highlightColor: "#B6B6B6"
  }, children);
};
Theme.defaultProps = defaultThemeProps;
var _default = Theme;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultThemeProps, "defaultThemeProps", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(Theme, "Theme", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8330:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Theme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8009);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _Theme__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 6094:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "k": () => (/* binding */ ListData)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4030);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9030);
/* harmony import */ var _slices_beranda_listData__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8137);
/* harmony import */ var _assets_icons_ic_search_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5449);
/* harmony import */ var _components_select_select_tim__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1943);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};






var ListData = function ListData() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C)(function (state) {
      return state.listData;
    }),
    data = _useAppSelector.data,
    meta = _useAppSelector.meta;
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z)(_useState, 2),
    searchValue = _useState2[0],
    setSearchValue = _useState2[1];
  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState4 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z)(_useState3, 2),
    filterTim = _useState4[0],
    setFilterTim = _useState4[1];
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: searchValue,
    timCode: filterTim
  };
  var onChangePage = function onChangePage(page) {
    dispatch((0,_slices_beranda_listData__WEBPACK_IMPORTED_MODULE_3__/* .setLoadMore */ .Uq)(true));
    dispatch((0,_slices_beranda_listData__WEBPACK_IMPORTED_MODULE_3__/* .fetchListDataTabel */ .UW)({
      page: page,
      size: tabelParams.size,
      order: '',
      sort: '',
      search: searchValue
    }));
  };
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_beranda_listData__WEBPACK_IMPORTED_MODULE_3__/* .fetchListDataTabel */ .UW)(tabelParams));
  }, [searchValue, filterTim]);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-between items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Data Admin"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex space-x-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_select_select_tim__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z, {
    onChange: setFilterTim
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center items-center p-2 bg-[#DEE9FF] rounded-lg space-x-3 "
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: _assets_icons_ic_search_svg__WEBPACK_IMPORTED_MODULE_4__,
    alt: "search"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    className: "outline-none text-black bg-transparent",
    placeholder: "Cari Nama",
    onChange: function onChange(e) {
      return setSearchValue(e.target.value);
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_table__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
    data: data || data,
    loading: false,
    meta: meta,
    onChangePage: onChangePage,
    paginationPerPage: tabelParams.size
  }));
};
__signature__(ListData, "useAppDispatch{dispatch}\nuseAppSelector{{ data, meta }}\nuseState{[ searchValue, setSearchValue ]('')}\nuseState{[filterTim, setFilterTim]('')}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(ListData, "ListData", "D:\\Project\\Base_Code\\src\\features\\detail_data\\listData.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3275:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "W": () => (/* binding */ CellSkeleton)
});

// UNUSED EXPORTS: default

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/react-loading-skeleton/dist/index.mjs
var dist = __webpack_require__(4849);
// EXTERNAL MODULE: ./src/components/skeleton/index.tsx
var skeleton = __webpack_require__(8330);
;// CONCATENATED MODULE: ./src/features/detail_data/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"summaryCard":"KX5OfeGC_XqVvjmtZ1rg","wrapperCard":"f5goZi8g2bPueN2sG6bn","wrapperGrafik":"j4cm4TxgG92kqMX2n8gs","table":"gePyoxy7_OFAAHNbU3gk","header":"zL4hRI27gl948W1FbYvG","row":"aTXwZXHD0kImK7d1dQrD"});
;// CONCATENATED MODULE: ./src/features/detail_data/skeleton.tsx
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var defaultCellProps = {
  children: ''
};
var CellSkeleton = function CellSkeleton(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react.createElement(skeleton/* default */.Z, null, /*#__PURE__*/react.createElement("span", {
    className: "relative"
  }, children, /*#__PURE__*/react.createElement("div", {
    className: "absolute top-0 right-0 bottom-0 left-0"
  }, /*#__PURE__*/react.createElement(dist/* default */.Z, {
    height: "100%",
    width: "100%"
  }))));
};
CellSkeleton.defaultProps = defaultCellProps;
var defaultTableProps = {
  numbering: true,
  header: true,
  column: 5,
  row: 5
};
var TableSkeleton = function TableSkeleton(_ref2) {
  var header = _ref2.header,
    numbering = _ref2.numbering,
    column = _ref2.column,
    row = _ref2.row;
  var gridTemplateColumns = '';
  var xAxis = [];
  var yAxis = [];
  if (numbering) {
    gridTemplateColumns = '5em ';
    xAxis.push(-1);
  }
  if (column) {
    for (var i = 0; i < column; i++) {
      xAxis.push(i);
      gridTemplateColumns += '1fr ';
    }
  }
  if (row) {
    for (var _i = 0; _i < row; _i++) {
      yAxis.push(_i);
    }
  }
  return /*#__PURE__*/react.createElement(skeleton/* default */.Z, null, header && /*#__PURE__*/react.createElement("div", {
    className: style_module.header,
    style: {
      gridTemplateColumns: gridTemplateColumns
    }
  }, column && xAxis.map(function (x) {
    return /*#__PURE__*/react.createElement(dist/* default */.Z, {
      height: "1em",
      key: x
    });
  })), yAxis.length && yAxis.map(function (y) {
    return /*#__PURE__*/react.createElement("div", {
      className: style_module.row,
      style: {
        gridTemplateColumns: gridTemplateColumns
      },
      key: y
    }, xAxis.length && xAxis.map(function (x) {
      return /*#__PURE__*/react.createElement(dist/* default */.Z, {
        height: "1em",
        key: x
      });
    }));
  }));
};
TableSkeleton.defaultProps = defaultTableProps;
var _default = TableSkeleton;
/* harmony default export */ const detail_data_skeleton = ((/* unused pure expression or super */ null && (_default)));
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultCellProps, "defaultCellProps", "D:\\Project\\Base_Code\\src\\features\\detail_data\\skeleton.tsx");
  reactHotLoader.register(CellSkeleton, "CellSkeleton", "D:\\Project\\Base_Code\\src\\features\\detail_data\\skeleton.tsx");
  reactHotLoader.register(defaultTableProps, "defaultTableProps", "D:\\Project\\Base_Code\\src\\features\\detail_data\\skeleton.tsx");
  reactHotLoader.register(TableSkeleton, "TableSkeleton", "D:\\Project\\Base_Code\\src\\features\\detail_data\\skeleton.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\detail_data\\skeleton.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 4030:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_data_table_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(44);
/* harmony import */ var _skeleton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3275);
/* harmony import */ var _components_popUp_editAttendance__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(231);
/* harmony import */ var _assets_icons_edit_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(839);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};





var defaultProps = {
  loading: false,
  paginationPerPage: 10
};
var TableComponent = function TableComponent(_ref) {
  var data = _ref.data,
    loading = _ref.loading,
    paginationPerPage = _ref.paginationPerPage,
    meta = _ref.meta,
    onChangePage = _ref.onChangePage;
  var loadingCell = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(function (cell) {
    return loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_skeleton__WEBPACK_IMPORTED_MODULE_2__/* .CellSkeleton */ .W, null, cell) : cell;
  }, [loading]);
  var _React$useState = react__WEBPACK_IMPORTED_MODULE_0__.useState(false),
    _React$useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z)(_React$useState, 2),
    modalOpen = _React$useState2[0],
    setModalOpen = _React$useState2[1];
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z)(_useState, 2),
    selectedNIK = _useState2[0],
    setSelectedNIK = _useState2[1];
  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState4 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z)(_useState3, 2),
    selectedName = _useState4[0],
    setSelectedName = _useState4[1];
  var columns = [{
    name: 'Nama',
    cell: function cell(row) {
      return loadingCell(row.name);
    }
  }, {
    name: 'NIK',
    cell: function cell(row) {
      return loadingCell(row.nik);
    }
  }, {
    name: 'TPS',
    cell: function cell(row) {
      return loadingCell(row.tps);
    }
  }, {
    name: 'Keyakinan',
    cell: function cell(row) {
      return loadingCell(row.confidence);
    }
  }, {
    name: 'Verifikasi',
    cell: function cell(row) {
      return loadingCell(row.verifikasi ? 'Terverifikasi' : 'Belum Terverifikasi');
    }
  }, {
    name: 'Aksi',
    cell: function cell(row) {
      return loadingCell( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
        className: "flex space-x-1"
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
        className: "cursor-pointer",
        role: "presentation",
        onClick: function onClick() {
          setSelectedName(row.name);
          setSelectedNIK(row.nik);
          setModalOpen(true);
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
        src: _assets_icons_edit_svg__WEBPACK_IMPORTED_MODULE_4__,
        alt: "eye"
      }))));
    },
    width: '5rem'
  }];
  var customStyles = {
    table: {
      style: {
        backgroundColor: 'none'
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);'
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_data_table_component__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .ZP, {
    columns: columns,
    data: data,
    customStyles: customStyles,
    pagination: true,
    paginationServer: true,
    paginationPerPage: paginationPerPage,
    paginationDefaultPage: meta === null || meta === void 0 ? void 0 : meta.page,
    paginationTotalRows: meta === null || meta === void 0 ? void 0 : meta.totalData,
    onChangePage: onChangePage,
    paginationComponentOptions: {
      noRowsPerPage: true,
      rangeSeparatorText: 'dari'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_popUp_editAttendance__WEBPACK_IMPORTED_MODULE_3__/* .EditAttendanceModal */ .j, {
    selectedNIK: selectedNIK,
    selectedName: selectedName,
    modalOpen: modalOpen,
    setModalOpen: setModalOpen
  }));
};
__signature__(TableComponent, "useCallback{loadingCell}\nuseState{[ modalOpen, setModalOpen ](false)}\nuseState{[ selectedNIK, setSelectedNIK ]('')}\nuseState{[ selectedName, setSelectedName ]('')}");
TableComponent.defaultProps = defaultProps;
var _default = TableComponent;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultProps, "defaultProps", "D:\\Project\\Base_Code\\src\\features\\detail_data\\table.tsx");
  reactHotLoader.register(TableComponent, "TableComponent", "D:\\Project\\Base_Code\\src\\features\\detail_data\\table.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\detail_data\\table.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3086:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "$": () => (/* binding */ DetailData)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _features_detail_data_listData__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6094);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var DetailData = function DetailData() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_detail_data_listData__WEBPACK_IMPORTED_MODULE_1__/* .ListData */ .k, null);
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(DetailData, "DetailData", "D:\\Project\\Base_Code\\src\\pages\\detail_data\\detailData.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8279:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _detailData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3086);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _detailData__WEBPACK_IMPORTED_MODULE_0__/* .DetailData */ .$;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\pages\\detail_data\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9928:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/36f57921ad40b2051363.svg";

/***/ }),

/***/ 839:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/a5c36060e8087cff009e.svg";

/***/ }),

/***/ 5449:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/b4ecf84f6fabb207bf70.svg";

/***/ })

}]);
//# sourceMappingURL=279.bf61a8717772ffa1abc8.bundle.js.map
"use strict";
(self["webpackChunkreact_ts_default"] = self["webpackChunkreact_ts_default"] || []).push([[133],{

/***/ 4586:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ login_form)
});

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/slicedToArray.js + 3 modules
var slicedToArray = __webpack_require__(885);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/formik/dist/formik.esm.js + 121 modules
var formik_esm = __webpack_require__(1054);
// EXTERNAL MODULE: ./src/components/spinnerButton/index.tsx
var spinnerButton = __webpack_require__(3370);
// EXTERNAL MODULE: ./src/assets/icons/fi-sr-eye.svg
var fi_sr_eye = __webpack_require__(5234);
// EXTERNAL MODULE: ./src/assets/icons/fi-sr-eye-crossed.svg
var fi_sr_eye_crossed = __webpack_require__(6538);
;// CONCATENATED MODULE: ./src/components/form/login/styles.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const styles_module = ({"inputGruop":"uYS8eeRQ22xjsXmOpQ8C","submit":"gZEbLMXyJmTrYLwqpMwH"});
;// CONCATENATED MODULE: ./src/components/form/login/form.tsx
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};






var initialValues = {
  username: '',
  password: ''
};
var FormComponent = function FormComponent(_ref) {
  var handleSubmit = _ref.handleSubmit,
    isLoading = _ref.isLoading,
    error = _ref.error;
  var _useState = (0,react.useState)('password'),
    _useState2 = (0,slicedToArray/* default */.Z)(_useState, 2),
    typePsw = _useState2[0],
    setTypePsw = _useState2[1];
  var toggleType = function toggleType(type) {
    return type === 'password' ? 'text' : 'password';
  };
  var validate = function validate(values) {
    var errors = {};
    if (values.username && values.username.length < 3) {
      Object.assign(errors, {
        username: 'Username minimal 3 karakter'
      });
    }
    if (values.password && values.password.length < 8) {
      Object.assign(errors, {
        password: 'Password minimal 8 karakter'
      });
    }
    return errors;
  };
  return /*#__PURE__*/react.createElement(formik_esm/* Formik */.J9, {
    initialValues: initialValues,
    validate: validate
  }, function (_ref2) {
    var values = _ref2.values;
    var disabled = true;
    if (values.username && values.password) disabled = false;
    if (values.username && values.password) disabled = false;
    return /*#__PURE__*/react.createElement(formik_esm/* Form */.l0, null, /*#__PURE__*/react.createElement("div", {
      className: styles_module.inputGruop
    }, /*#__PURE__*/react.createElement("div", {
      className: "flex-grow"
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "username"
    }, "Nama Pengguna"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      type: "text",
      name: "username",
      id: "username",
      autoComplete: "off"
    }))), /*#__PURE__*/react.createElement("div", {
      className: styles_module.inputGruop
    }, /*#__PURE__*/react.createElement("div", null, /*#__PURE__*/react.createElement("label", {
      htmlFor: "password"
    }, "Kata Sandi"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      type: typePsw,
      name: "password",
      id: "password",
      autoComplete: "off"
    })), /*#__PURE__*/react.createElement("div", {
      className: "self-end",
      onClick: function onClick() {
        return setTypePsw(toggleType(typePsw));
      },
      tabIndex: 0,
      onKeyPress: function onKeyPress() {},
      role: "button"
    }, /*#__PURE__*/react.createElement("img", {
      className: "cursor-pointer",
      alt: "icon eye open",
      src: typePsw === 'password' ? fi_sr_eye_crossed : fi_sr_eye
    }))), error !== undefined ? /*#__PURE__*/react.createElement("p", {
      className: "text-sm text-red text-center"
    }, error === null || error === void 0 ? void 0 : error.message) : '', /*#__PURE__*/react.createElement("button", {
      type: "button",
      className: styles_module.submit,
      disabled: isLoading === true || disabled,
      onClick: function onClick() {
        return handleSubmit(values);
      }
    }, isLoading === true ? /*#__PURE__*/react.createElement("div", {
      className: "flex justify-center items-center"
    }, /*#__PURE__*/react.createElement(spinnerButton/* default */.Z, null)) : /*#__PURE__*/react.createElement("div", null, "Masuk")));
  });
};

// FormComponent.defaultProps = defaultProps
__signature__(FormComponent, "useState{[typePsw, setTypePsw]('password')}");
var _default = FormComponent;
/* harmony default export */ const login_form = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(initialValues, "initialValues", "D:\\Project\\Base_Code\\src\\components\\form\\login\\form.tsx");
  reactHotLoader.register(FormComponent, "FormComponent", "D:\\Project\\Base_Code\\src\\components\\form\\login\\form.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\form\\login\\form.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3370:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _spinnerButton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9874);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _spinnerButton__WEBPACK_IMPORTED_MODULE_0__/* .SpinnerButton */ .m;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\spinnerButton\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9874:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "m": () => (/* binding */ SpinnerButton)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var SpinnerButton = function SpinnerButton() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    className: "mr-3 h-5 w-5 animate-spin text-white",
    xmlns: "http://www.w3.org/2000/svg",
    fill: "none",
    viewBox: "0 0 24 24"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("circle", {
    className: "opacity-25",
    cx: "12",
    cy: "12",
    r: "10",
    stroke: "currentColor",
    strokeWidth: "4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("path", {
    className: "opacity-75",
    fill: "currentColor",
    d: "M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
  }));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(SpinnerButton, "SpinnerButton", "D:\\Project\\Base_Code\\src\\components\\spinnerButton\\spinnerButton.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9133:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _login__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8744);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _login__WEBPACK_IMPORTED_MODULE_0__/* .Login */ .m;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\pages\\login\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8744:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "m": () => (/* binding */ Login)
});

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(5861);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/slicedToArray.js + 3 modules
var slicedToArray = __webpack_require__(885);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(4687);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./src/api/authService.ts
var authService = __webpack_require__(8770);
// EXTERNAL MODULE: ./src/components/form/login/form.tsx + 1 modules
var login_form = __webpack_require__(4586);
;// CONCATENATED MODULE: ./src/pages/login/styles.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const styles_module = ({"sections":"eTzkJCWB0QsHUVMYA6Ck","container":"bLxiNe7F83EifeBe3hEK","logo":"MktkqFWptNknnnN6w_kd","title":"lxX2q0Pk4IYcbWytWBNi","dontHaveAccount":"y1MCYn3d3S_Ok4CQTP7M"});
// EXTERNAL MODULE: ./src/assets/images/voting-box 1.png
var voting_box_1 = __webpack_require__(7463);
;// CONCATENATED MODULE: ./src/pages/login/login.tsx
/* module decorator */ module = __webpack_require__.hmd(module);


(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};





var Login = function Login() {
  var _useState = (0,react.useState)(false),
    _useState2 = (0,slicedToArray/* default */.Z)(_useState, 2),
    isLoading = _useState2[0],
    setIsLoading = _useState2[1];
  var _useState3 = (0,react.useState)(),
    _useState4 = (0,slicedToArray/* default */.Z)(_useState3, 2),
    errorRes = _useState4[0],
    setErrorRes = _useState4[1];
  function redirect() {
    window.location.href = "/beranda";
  }
  var authLogin = /*#__PURE__*/function () {
    var _ref = (0,asyncToGenerator/* default */.Z)( /*#__PURE__*/regenerator_default().mark(function _callee(params) {
      var response;
      return regenerator_default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              setIsLoading(true);
              _context.next = 4;
              return (0,authService/* login */.x4)(params);
            case 4:
              response = _context.sent;
              localStorage.setItem('token_dvt', response.data.accessToken);
              localStorage.setItem('exp_token_dvt', response.data.expAccessToken);
              localStorage.setItem('role_dvt', response.data.userType);
              setIsLoading(false);
              redirect();
              return _context.abrupt("return", true);
            case 13:
              _context.prev = 13;
              _context.t0 = _context["catch"](0);
              setErrorRes(_context.t0);
              setIsLoading(false);
              return _context.abrupt("return", false);
            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 13]]);
    }));
    return function authLogin(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  var handleSubmit = function handleSubmit(params) {
    authLogin(params);
  };
  return /*#__PURE__*/react.createElement("section", {
    className: styles_module.sections
  }, /*#__PURE__*/react.createElement("div", {
    className: "w-[400px]"
  }, /*#__PURE__*/react.createElement("div", {
    className: styles_module.container
  }, /*#__PURE__*/react.createElement("div", {
    className: "grid justify-center pb-8"
  }, /*#__PURE__*/react.createElement("div", {
    className: styles_module.logo
  }, /*#__PURE__*/react.createElement("img", {
    alt: "vote",
    src: voting_box_1
  }), /*#__PURE__*/react.createElement("div", {
    className: styles_module.title
  }, "Digital Vooters"), /*#__PURE__*/react.createElement("p", {
    className: "text-black font-semibold"
  }, "Masuk untuk melanjutkan"))), /*#__PURE__*/react.createElement(login_form/* default */.Z, {
    handleSubmit: handleSubmit,
    isLoading: isLoading,
    error: errorRes
  }))));
};
__signature__(Login, "useState{[ isLoading, setIsLoading ](false)}\nuseState{[ errorRes , setErrorRes ]}");

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(Login, "Login", "D:\\Project\\Base_Code\\src\\pages\\login\\login.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 6538:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/42bfbbad88718e2c2ae6.svg";

/***/ }),

/***/ 5234:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/4d985c86342da88195a8.svg";

/***/ }),

/***/ 7463:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/1800576771e1e2828950.png";

/***/ })

}]);
//# sourceMappingURL=133.97fe686e2d16017766ef.bundle.js.map
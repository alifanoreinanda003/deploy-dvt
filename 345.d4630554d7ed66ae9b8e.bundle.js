"use strict";
(self["webpackChunkreact_ts_default"] = self["webpackChunkreact_ts_default"] || []).push([[345],{

/***/ 4876:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "y": () => (/* binding */ MonthlyChart)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5376);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1951);
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2181);
/* harmony import */ var date_fns__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(5021);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9030);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4467);
/* harmony import */ var _slices_beranda_superAdmin_dataLineChart__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3608);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};








chart_js__WEBPACK_IMPORTED_MODULE_1__/* .Chart.register */ .kL.register(chart_js__WEBPACK_IMPORTED_MODULE_1__/* .CategoryScale */ .uw, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .LinearScale */ .f$, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .PointElement */ .od, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .LineElement */ .jn, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .Title */ .Dx, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .Tooltip */ .u, chart_js__WEBPACK_IMPORTED_MODULE_1__/* .Legend */ .De);
var MonthlyChart = function MonthlyChart() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C)(function (state) {
      return state.lineChartSuperAdmin;
    }),
    dataGrafik = _useAppSelector.data;
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z)(_useState, 2),
    month = _useState2[0],
    setMonth = _useState2[1];
  var mapDataGrafik = dataGrafik === null || dataGrafik === void 0 ? void 0 : dataGrafik.map(function (item) {
    return item.count;
  });
  var dateDataGrafik = dataGrafik === null || dataGrafik === void 0 ? void 0 : dataGrafik.map(function (item) {
    return (0,date_fns__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z)(item.date);
  });
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_beranda_superAdmin_dataLineChart__WEBPACK_IMPORTED_MODULE_4__/* .fetchLineChartSuperAdmin */ .WH)(month));
  }, [month]);
  var data = {
    datasets: [{
      data: mapDataGrafik,
      borderColor: 'rgb(53, 162, 235)',
      backgroundColor: 'rgba(53, 162, 235, 0.5)'
    }],
    labels: dateDataGrafik
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "md:w-[69.9rem] bg-white mt-3 p-6 rounded-lg shadow-lg  space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-between items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Grafik Data Bulanan"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_select__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .ZP, {
    styles: _utils__WEBPACK_IMPORTED_MODULE_3__/* .selectStyles */ .X,
    options: _utils__WEBPACK_IMPORTED_MODULE_3__/* .selectOptions */ .x,
    onChange: function onChange(e) {
      return setMonth((e === null || e === void 0 ? void 0 : e.value) === undefined ? '' : e.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pt-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_chartjs_2__WEBPACK_IMPORTED_MODULE_8__/* .Line */ .x1, {
    data: data,
    options: {
      scales: {
        y: {
          beginAtZero: true,
          ticks: {
            precision: 0
          }
        }
      },
      responsive: true,
      plugins: {
        legend: {
          display: false
        }
      }
    }
  })));
};
__signature__(MonthlyChart, "useAppDispatch{dispatch}\nuseAppSelector{{ data: dataGrafik }}\nuseState{[month,setMonth]('')}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(MonthlyChart, "MonthlyChart", "D:\\Project\\Base_Code\\src\\features\\dashboard\\monthlyChart.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8233:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "E": () => (/* binding */ Summary)
});

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
;// CONCATENATED MODULE: ./src/features/dashboard/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"summaryCard":"Kbc1kDhoIdLyO2NrvNKU","wrapperCard":"J5DWj0OBVlADozDhtEF5","wrapperGrafik":"BBTwZOg4LQkveQOooTRC","table":"KiwtlshaNsqPYkIyyTUa","header":"G7rN0iLPqg8urmsmgxwR","row":"WRdpcu2uYPg_gbt9TA75"});
// EXTERNAL MODULE: ./src/redux/hooks.ts
var hooks = __webpack_require__(9030);
// EXTERNAL MODULE: ./src/slices/beranda_superAdmin/summarySuperAdmin.tsx
var summarySuperAdmin = __webpack_require__(5688);
;// CONCATENATED MODULE: ./src/features/dashboard/summaryCard.tsx
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var Summary = function Summary() {
  var dispatch = (0,hooks/* useAppDispatch */.T)();
  var _useAppSelector = (0,hooks/* useAppSelector */.C)(function (state) {
      return state.summarySuperAdmin;
    }),
    data = _useAppSelector.data;
  console.log(data);
  (0,react.useEffect)(function () {
    dispatch((0,summarySuperAdmin/* fetchSummarySuperAdmin */.Z3)());
  }, []);
  var menu = [{
    id: 1,
    title: 'Admin Terdaftar',
    amount: data === null || data === void 0 ? void 0 : data.totalAdmin,
    icon: __webpack_require__(4957)
  }, {
    id: 2,
    title: 'Total Data Terferifikasi',
    amount: data === null || data === void 0 ? void 0 : data.totalVerification,
    icon: __webpack_require__(3126)
  }, {
    id: 3,
    title: 'Total Input Data',
    amount: data === null || data === void 0 ? void 0 : data.totalData,
    icon: __webpack_require__(7658)
  }];
  return /*#__PURE__*/react.createElement("section", null, /*#__PURE__*/react.createElement("div", {
    className: style_module.wrapperCard
  }, menu.map(function (item) {
    return /*#__PURE__*/react.createElement("div", {
      className: style_module.summaryCard,
      key: item.id
    }, /*#__PURE__*/react.createElement("div", {
      className: "flex justify-center items-center w-fit h-fit"
    }, /*#__PURE__*/react.createElement("img", {
      src: item.icon,
      alt: ""
    })), /*#__PURE__*/react.createElement("div", {
      className: "grid"
    }, /*#__PURE__*/react.createElement("span", {
      className: "text-2xl font-semibold text-primary-2"
    }, item.amount), /*#__PURE__*/react.createElement("span", {
      className: "text-base text-secondary-3"
    }, item.title)));
  })));
};
__signature__(Summary, "useAppDispatch{dispatch}\nuseAppSelector{{ data }}\nuseEffect{}", function () {
  return [hooks/* useAppDispatch */.T, hooks/* useAppSelector */.C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(Summary, "Summary", "D:\\Project\\Base_Code\\src\\features\\dashboard\\summaryCard.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 4467:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "X": () => (/* binding */ selectStyles),
/* harmony export */   "x": () => (/* binding */ selectOptions)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4942);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0,_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z)(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};
var selectOptions = [{
  value: "1",
  label: "Januari"
}, {
  value: "2",
  label: "Februari"
}, {
  value: "3",
  label: "Maret"
}, {
  value: "4",
  label: "April"
}, {
  value: "5",
  label: "Mei"
}, {
  value: "6",
  label: "Juni"
}, {
  value: "7",
  label: "Juli"
}, {
  value: "8",
  label: "Agustus"
}, {
  value: "9",
  label: "September"
}, {
  value: "10",
  label: "Oktober"
}, {
  value: "11",
  label: "November"
}, {
  value: "12",
  label: "Desember"
}];
var selectStyles = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  singleValue: function singleValue(provided) {
    return _objectSpread(_objectSpread({}, provided), {}, {
      height: '100%',
      color: 'black',
      paddingTop: '3px'
    });
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  option: function option(provided) {
    return _objectSpread(_objectSpread({}, provided), {}, {
      color: 'black'
    });
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  control: function control(provided) {
    return _objectSpread(_objectSpread({}, provided), {}, {
      color: 'black',
      width: 120
    });
  }
};
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(selectOptions, "selectOptions", "D:\\Project\\Base_Code\\src\\features\\dashboard\\utils.ts");
  reactHotLoader.register(selectStyles, "selectStyles", "D:\\Project\\Base_Code\\src\\features\\dashboard\\utils.ts");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8043:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "l": () => (/* binding */ ValidationChart)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_chartjs_2__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5376);
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2181);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1951);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4467);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9030);
/* harmony import */ var _slices_beranda_superAdmin_dataPieChart__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7252);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};







chart_js__WEBPACK_IMPORTED_MODULE_1__/* .Chart.register */ .kL.register(chart_js__WEBPACK_IMPORTED_MODULE_1__/* .ArcElement */ .qi);
var ValidationChart = function ValidationChart() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppSelector */ .C)(function (state) {
      return state.pieChartSuperAdmin;
    }),
    data = _useAppSelector.data;
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(''),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_5__/* ["default"] */ .Z)(_useState, 2),
    month = _useState2[0],
    setMonth = _useState2[1];
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_beranda_superAdmin_dataPieChart__WEBPACK_IMPORTED_MODULE_4__/* .fetchPieChartSuperAdmin */ .Mr)(month));
  }, [month]);
  var dataPie = {
    datasets: [{
      data: [data === null || data === void 0 ? void 0 : data.verification, data === null || data === void 0 ? void 0 : data.notVerification],
      backgroundColor: ['rgba(53, 162, 235, 0.5)', 'rgba(169,169,169, 0.5)']
    }],
    labels: ['Data Valid', 'Data Invalid']
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "h-fit bg-white mt-3 p-6 rounded-lg shadow-lg space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-between items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Grafik Validasi"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_select__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .ZP, {
    styles: _utils__WEBPACK_IMPORTED_MODULE_2__/* .selectStyles */ .X,
    options: _utils__WEBPACK_IMPORTED_MODULE_2__/* .selectOptions */ .x,
    onChange: function onChange(e) {
      return setMonth((e === null || e === void 0 ? void 0 : e.value) === undefined ? '' : e.value);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pt-7"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_chartjs_2__WEBPACK_IMPORTED_MODULE_7__/* .Doughnut */ .$I, {
    data: dataPie,
    options: {
      responsive: true,
      plugins: {
        legend: {
          display: true,
          position: 'bottom'
        }
      }
    }
  })));
};
__signature__(ValidationChart, "useAppDispatch{dispatch}\nuseAppSelector{{ data }}\nuseState{[month,setMonth]('')}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_3__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(ValidationChart, "ValidationChart", "D:\\Project\\Base_Code\\src\\features\\dashboard\\validationChart.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 2027:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "A": () => (/* binding */ Dashboard)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _features_dashboard_summaryCard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8233);
/* harmony import */ var _features_dashboard_monthlyChart__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(4876);
/* harmony import */ var _features_dashboard_validationChart__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8043);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var Dashboard = function Dashboard() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_dashboard_summaryCard__WEBPACK_IMPORTED_MODULE_1__/* .Summary */ .E, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "\r laptop:w-[47.5rem] \r desktop:w-[63.5rem] \r notebook:w-[69rem] \r gaming:w-full\r md:flex md:justify-between space-x-4 z-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_dashboard_monthlyChart__WEBPACK_IMPORTED_MODULE_2__/* .MonthlyChart */ .y, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_dashboard_validationChart__WEBPACK_IMPORTED_MODULE_3__/* .ValidationChart */ .l, null)));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(Dashboard, "Dashboard", "D:\\Project\\Base_Code\\src\\pages\\dashboard_superAdmin\\dashboard.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 9345:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _dashboard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2027);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _dashboard__WEBPACK_IMPORTED_MODULE_0__/* .Dashboard */ .A;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\pages\\dashboard_superAdmin\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3126:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/c1a95404c1c5bc4f5731.png";

/***/ }),

/***/ 7658:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/0ca1f928231736d668d9.png";

/***/ }),

/***/ 4957:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/adde8afa7d71309db4ed.png";

/***/ })

}]);
//# sourceMappingURL=345.d4630554d7ed66ae9b8e.bundle.js.map
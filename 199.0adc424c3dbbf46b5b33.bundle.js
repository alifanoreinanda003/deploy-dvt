"use strict";
(self["webpackChunkreact_ts_default"] = self["webpackChunkreact_ts_default"] || []).push([[199],{

/***/ 5624:
/***/ ((module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ add_data_form)
});

// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__(4942);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js
var asyncToGenerator = __webpack_require__(5861);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/helpers/esm/slicedToArray.js + 3 modules
var slicedToArray = __webpack_require__(885);
// EXTERNAL MODULE: ./node_modules/@babel/runtime/regenerator/index.js
var regenerator = __webpack_require__(4687);
var regenerator_default = /*#__PURE__*/__webpack_require__.n(regenerator);
// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__(7294);
// EXTERNAL MODULE: ./node_modules/formik/dist/formik.esm.js + 121 modules
var formik_esm = __webpack_require__(1054);
// EXTERNAL MODULE: ./src/api/dashboard.ts
var dashboard = __webpack_require__(7099);
// EXTERNAL MODULE: ./src/components/popUp/informationAlert.tsx
var informationAlert = __webpack_require__(36);
;// CONCATENATED MODULE: ./src/components/form/add_data/style.module.css
// extracted by mini-css-extract-plugin
/* harmony default export */ const style_module = ({"form":"jBAGmjU2e8lfSmQ7mPBu","groupField":"PQF9PWyL8BUI5d5wQPpX","reset":"QcLYWAnNxtisKi_Qa73v","submit":"r8oaxFZSKUaHGBUs4cBe"});
// EXTERNAL MODULE: ./src/slices/beranda/listData.tsx
var listData = __webpack_require__(8137);
// EXTERNAL MODULE: ./src/components/form/add_data/utils.tsx
var utils = __webpack_require__(3001);
;// CONCATENATED MODULE: ./src/components/form/add_data/form.tsx
/* module decorator */ module = __webpack_require__.hmd(module);



(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { (0,defineProperty/* default */.Z)(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};







var FormComponent = function FormComponent() {
  var initialValues = {
    timName: '',
    nik: '',
    name: '',
    phone: ''
  };
  var tabelParams = {
    page: 1,
    size: 10,
    order: '',
    sort: '',
    search: ''
  };
  var _useState = (0,react.useState)(false),
    _useState2 = (0,slicedToArray/* default */.Z)(_useState, 2),
    openModal = _useState2[0],
    setOpenModal = _useState2[1];
  var _useState3 = (0,react.useState)(''),
    _useState4 = (0,slicedToArray/* default */.Z)(_useState3, 2),
    message = _useState4[0],
    setMessage = _useState4[1];
  var _useState5 = (0,react.useState)(false),
    _useState6 = (0,slicedToArray/* default */.Z)(_useState5, 2),
    isSuccess = _useState6[0],
    setIsSuccess = _useState6[1];
  var handleSubmit = /*#__PURE__*/function () {
    var _ref = (0,asyncToGenerator/* default */.Z)( /*#__PURE__*/regenerator_default().mark(function _callee(formValue) {
      return regenerator_default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return (0,dashboard/* addData */.Id)(formValue);
            case 3:
              (0,listData/* fetchListDataTabel */.UW)(tabelParams);
              setIsSuccess(true);
              setOpenModal(true);
              setMessage('Data Berhasil di Input');
              _context.next = 14;
              break;
            case 9:
              _context.prev = 9;
              _context.t0 = _context["catch"](0);
              setIsSuccess(false);
              setOpenModal(true);
              setMessage(_context.t0.message);
            case 14:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[0, 9]]);
    }));
    return function handleSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();
  return /*#__PURE__*/react.createElement("div", null, /*#__PURE__*/react.createElement(formik_esm/* Formik */.J9, {
    initialValues: initialValues,
    validate: utils/* validate */.G,
    onSubmit: function onSubmit(value) {
      handleSubmit(_objectSpread(_objectSpread({}, value), {}, {
        timName: value.timName.toLowerCase()
      }));
    }
  }, function (_ref2) {
    var values = _ref2.values,
      errors = _ref2.errors,
      touched = _ref2.touched;
    var disabled = Object.values(errors).toString() !== '';
    return /*#__PURE__*/react.createElement(formik_esm/* Form */.l0, null, /*#__PURE__*/react.createElement("div", {
      className: style_module.form
    }, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "timName"
    }, "Tim"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "timName",
      id: "tim",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Nama Tim"
    }), /*#__PURE__*/react.createElement("span", null)), errors.timName && touched.timName ? /*#__PURE__*/react.createElement("div", {
      className: "italic text-sm text-red"
    }, errors.timName) : null, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "nik"
    }, "NIK"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "nik",
      placeholder: "Masukan NIK"
    }), /*#__PURE__*/react.createElement("span", null)), errors.nik && touched.nik ? /*#__PURE__*/react.createElement("div", {
      className: "italic text-sm text-red"
    }, errors.nik) : null, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "nama"
    }, "Nama"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "name",
      id: "nama",
      type: "text",
      autoComplete: "off",
      placeholder: "Masukan Nama"
    }), /*#__PURE__*/react.createElement("span", null)), errors.name && touched.name ? /*#__PURE__*/react.createElement("div", {
      className: "italic text-sm text-red"
    }, errors.name) : null, /*#__PURE__*/react.createElement("div", {
      className: style_module.groupField
    }, /*#__PURE__*/react.createElement("label", {
      htmlFor: "phone"
    }, "Telepon"), /*#__PURE__*/react.createElement(formik_esm/* Field */.gN, {
      name: "phone",
      placeholder: "Masukan Telepone"
    }), /*#__PURE__*/react.createElement("span", null)), errors.phone && touched.phone ? /*#__PURE__*/react.createElement("div", {
      className: "italic text-sm text-red"
    }, errors.phone) : null), /*#__PURE__*/react.createElement("div", {
      className: "flex justify-between items-center space-x-2 pt-4"
    }, /*#__PURE__*/react.createElement("button", {
      type: "reset",
      className: style_module.reset,
      onClick: function onClick() {}
    }, "Reset"), /*#__PURE__*/react.createElement("button", {
      type: "submit",
      className: style_module.submit,
      disabled: disabled
    }, "Submit")));
  }), /*#__PURE__*/react.createElement(informationAlert/* default */.Z, {
    isOpen: openModal,
    message: message,
    isSuccess: isSuccess,
    setOpenModal: setOpenModal
  }));
};
__signature__(FormComponent, "useState{[openModal, setOpenModal](false)}\nuseState{[message, setMessage]('')}\nuseState{[isSuccess, setIsSuccess](false)}");
var _default = FormComponent;
/* harmony default export */ const add_data_form = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(FormComponent, "FormComponent", "D:\\Project\\Base_Code\\src\\components\\form\\add_data\\form.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\form\\add_data\\form.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3001:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "G": () => (/* binding */ validate)
/* harmony export */ });
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};
var validate = function validate(values) {
  var errors = {};
  if (!values.timName) {
    Object.assign(errors, {
      timName: 'Nama tim wajib diisi'
    });
  }
  if (!values.name) {
    Object.assign(errors, {
      name: 'Nama wajib diisi'
    });
  }
  if (values.name && values.name.length < 3) {
    Object.assign(errors, {
      name: 'Nama minimal 3 karakter'
    });
  }
  if (!values.nik) {
    Object.assign(errors, {
      nik: 'NIK Wajib Diisi'
    });
  }
  if (values.nik && values.nik.length < 16) {
    Object.assign(errors, {
      nik: 'NIK wajib 16 karakter'
    });
  }
  if (values.phone && values.phone.length < 12) {
    Object.assign(errors, {
      phone: 'Nomor Telefon wajib 12 karakter'
    });
  }
  if (!values.phone) {
    Object.assign(errors, {
      phone: 'Nomor Telefon wajib diisi'
    });
  }
  return errors;
};
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(validate, "validate", "D:\\Project\\Base_Code\\src\\components\\form\\add_data\\utils.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 36:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_icons_ic_cloce_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2663);
/* harmony import */ var _assets_images_ic_success_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9881);
/* harmony import */ var _assets_images_ic_warning_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3386);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};





var InformationAlert = function InformationAlert(props) {
  var customStyle = {
    overlay: {
      backgroundColor: 'rgba(0, 0, 0, 0.50)'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#ffffff',
      outline: 'none',
      border: 'none',
      borderRadius: '10px'
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_1___default()), {
    ariaHideApp: false,
    isOpen: props.isOpen,
    style: customStyle
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_ic_cloce_svg__WEBPACK_IMPORTED_MODULE_2__,
    alt: "ic-close",
    onClick: function onClick() {
      props.setOpenModal(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-center items-center"
  }, props.isSuccess === true && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    className: "w-32",
    src: _assets_images_ic_success_png__WEBPACK_IMPORTED_MODULE_3__,
    alt: "icon-success"
  }), props.isSuccess === false && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    className: "w-32",
    src: _assets_images_ic_warning_png__WEBPACK_IMPORTED_MODULE_4__,
    alt: "icon-warning"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-7 font-semibold text-2xl text-[#091D60] "
  }, props.message)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "w-full pr-0 py-3 mt-4 font-medium rounded-lg bg-[#1877F2] text-white",
    onClick: function onClick() {
      props.setOpenModal(false);
    }
  }, "Oke"));
};
var _default = InformationAlert;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(InformationAlert, "InformationAlert", "D:\\Project\\Base_Code\\src\\components\\popUp\\informationAlert.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\popUp\\informationAlert.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8009:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4849);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var defaultThemeProps = {
  children: ''
};
var Theme = function Theme(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_1__/* .SkeletonTheme */ .y, {
    baseColor: "#5C6673",
    highlightColor: "#B6B6B6"
  }, children);
};
Theme.defaultProps = defaultThemeProps;
var _default = Theme;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultThemeProps, "defaultThemeProps", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(Theme, "Theme", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\Theme.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8330:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Theme__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8009);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _Theme__WEBPACK_IMPORTED_MODULE_0__/* ["default"] */ .Z;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\components\\skeleton\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 7883:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "y": () => (/* binding */ FormInput)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _components_form_add_data_form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5624);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};


var FormInput = function FormInput() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "md:w-[500px] h-fit bg-white mt-3 p-6 rounded-lg shadow-lg space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Form Input"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_form_add_data_form__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, null));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(FormInput, "FormInput", "D:\\Project\\Base_Code\\src\\features\\beranda\\formInput.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 8823:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "l": () => (/* binding */ EmployeeList)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1643);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9030);
/* harmony import */ var _slices_beranda_listDataTim__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8497);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var EmployeeList = function EmployeeList() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C)(function (state) {
      return state.listDataTim;
    }),
    data = _useAppSelector.data,
    meta = _useAppSelector.meta;
  var tabelParams = {
    page: 1,
    size: 10,
    verify: true,
    order: '',
    sort: '',
    search: ''
  };
  var onChangePage = function onChangePage(page) {
    dispatch((0,_slices_beranda_listDataTim__WEBPACK_IMPORTED_MODULE_3__/* .setLoadMore */ .Uq)(true));
    dispatch((0,_slices_beranda_listDataTim__WEBPACK_IMPORTED_MODULE_3__/* .fetchListDataTim */ .BL)({
      page: page,
      size: tabelParams.size,
      verify: true,
      order: '',
      sort: '',
      search: ''
    }));
  };
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_beranda_listDataTim__WEBPACK_IMPORTED_MODULE_3__/* .fetchListDataTim */ .BL)(tabelParams));
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "bg-white mt-3 p-6 rounded-lg shadow-lg w-full space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "text-xl text-[#091D60] font-semibold"
  }, "Tabel Data"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_table__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {
    data: data,
    loading: false,
    meta: meta
    // loadMore={loadmore}
    ,
    onChangePage: onChangePage,
    paginationPerPage: tabelParams.size
  }));
};
__signature__(EmployeeList, "useAppDispatch{dispatch}\nuseAppSelector{{ data,meta }}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(EmployeeList, "EmployeeList", "D:\\Project\\Base_Code\\src\\features\\beranda\\listData.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 3576:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "W": () => (/* binding */ CellSkeleton)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4849);
/* harmony import */ var _components_skeleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8330);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6659);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var defaultCellProps = {
  children: ''
};
var CellSkeleton = function CellSkeleton(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_skeleton__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "relative"
  }, children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "absolute top-0 right-0 bottom-0 left-0"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
    height: "100%",
    width: "100%"
  }))));
};
CellSkeleton.defaultProps = defaultCellProps;
var defaultTableProps = {
  numbering: true,
  header: true,
  column: 5,
  row: 5
};
var TableSkeleton = function TableSkeleton(_ref2) {
  var header = _ref2.header,
    numbering = _ref2.numbering,
    column = _ref2.column,
    row = _ref2.row;
  var gridTemplateColumns = '';
  var xAxis = [];
  var yAxis = [];
  if (numbering) {
    gridTemplateColumns = '5em ';
    xAxis.push(-1);
  }
  if (column) {
    for (var i = 0; i < column; i++) {
      xAxis.push(i);
      gridTemplateColumns += '1fr ';
    }
  }
  if (row) {
    for (var _i = 0; _i < row; _i++) {
      yAxis.push(_i);
    }
  }
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_components_skeleton__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, null, header && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: _style_module_css__WEBPACK_IMPORTED_MODULE_2__/* ["default"].header */ .Z.header,
    style: {
      gridTemplateColumns: gridTemplateColumns
    }
  }, column && xAxis.map(function (x) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
      height: "1em",
      key: x
    });
  })), yAxis.length && yAxis.map(function (y) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
      className: _style_module_css__WEBPACK_IMPORTED_MODULE_2__/* ["default"].row */ .Z.row,
      style: {
        gridTemplateColumns: gridTemplateColumns
      },
      key: y
    }, xAxis.length && xAxis.map(function (x) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_loading_skeleton__WEBPACK_IMPORTED_MODULE_3__/* ["default"] */ .Z, {
        height: "1em",
        key: x
      });
    }));
  }));
};
TableSkeleton.defaultProps = defaultTableProps;
var _default = TableSkeleton;
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ((/* unused pure expression or super */ null && (_default)));
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultCellProps, "defaultCellProps", "D:\\Project\\Base_Code\\src\\features\\beranda\\skeleton.tsx");
  reactHotLoader.register(CellSkeleton, "CellSkeleton", "D:\\Project\\Base_Code\\src\\features\\beranda\\skeleton.tsx");
  reactHotLoader.register(defaultTableProps, "defaultTableProps", "D:\\Project\\Base_Code\\src\\features\\beranda\\skeleton.tsx");
  reactHotLoader.register(TableSkeleton, "TableSkeleton", "D:\\Project\\Base_Code\\src\\features\\beranda\\skeleton.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\beranda\\skeleton.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 5874:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "E": () => (/* binding */ Summary)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6659);
/* harmony import */ var _redux_hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9030);
/* harmony import */ var _slices_beranda_summary__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8240);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var Summary = function Summary() {
  var dispatch = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T)();
  var _useAppSelector = (0,_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C)(function (state) {
      return state.summary;
    }),
    data = _useAppSelector.data;
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    dispatch((0,_slices_beranda_summary__WEBPACK_IMPORTED_MODULE_3__/* .fetchSummary */ .gZ)());
  }, []);
  var menu = [{
    id: 1,
    title: 'Kepercayaan Terbanyak',
    amount: (data === null || data === void 0 ? void 0 : data.totalConfidenc) || 0,
    icon: __webpack_require__(1874)
  }, {
    id: 2,
    title: 'Total Data Terferifikasi',
    amount: (data === null || data === void 0 ? void 0 : data.totalVerification) || 0,
    icon: __webpack_require__(3126)
  }, {
    id: 3,
    title: 'Total Input Data',
    amount: (data === null || data === void 0 ? void 0 : data.totalData) || 0,
    icon: __webpack_require__(7658)
  }];
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: _style_module_css__WEBPACK_IMPORTED_MODULE_1__/* ["default"].wrapperCard */ .Z.wrapperCard
  }, menu.map(function (item) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
      className: _style_module_css__WEBPACK_IMPORTED_MODULE_1__/* ["default"].summaryCard */ .Z.summaryCard,
      key: item.id
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
      className: "flex justify-center items-center w-fit h-fit"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
      src: item.icon,
      alt: ""
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
      className: "grid"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
      className: "text-2xl font-semibold text-primary-2"
    }, item.amount), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
      className: "text-base text-secondary-3"
    }, item.title)));
  })));
};
__signature__(Summary, "useAppDispatch{dispatch}\nuseAppSelector{{ data }}\nuseEffect{}", function () {
  return [_redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppDispatch */ .T, _redux_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useAppSelector */ .C];
});

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(Summary, "Summary", "D:\\Project\\Base_Code\\src\\features\\beranda\\summaryCard.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 1643:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(885);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var react_data_table_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(44);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3253);
/* harmony import */ var react_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _skeleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3576);
/* harmony import */ var _assets_icons_ic_cloce_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2663);
/* harmony import */ var _assets_icons_ic_delete_user_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9371);
/* module decorator */ module = __webpack_require__.hmd(module);

(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};






var defaultProps = {
  loading: false,
  paginationPerPage: 10
};
var TableComponent = function TableComponent(_ref) {
  var data = _ref.data,
    loading = _ref.loading,
    paginationPerPage = _ref.paginationPerPage,
    meta = _ref.meta,
    onChangePage = _ref.onChangePage;
  var loadingCell = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(function (cell) {
    return loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_skeleton__WEBPACK_IMPORTED_MODULE_3__/* .CellSkeleton */ .W, null, cell) : cell;
  }, [loading]);
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
    _useState2 = (0,_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_6__/* ["default"] */ .Z)(_useState, 2),
    modalOpen = _useState2[0],
    setModalOpen = _useState2[1];
  var columns = [{
    name: 'Nama',
    cell: function cell(row) {
      return loadingCell(row.name);
    }
  }, {
    name: 'NIK',
    cell: function cell(row) {
      return loadingCell(row.nik);
    }
  }, {
    name: 'TPS',
    cell: function cell(row) {
      return loadingCell(row.tps || '-');
    }
  }, {
    name: 'Nama TIM',
    cell: function cell(row) {
      return loadingCell(row.timName || '-');
    }
  }];
  var modalStyle = {
    overlay: {
      backgroundColor: '#ffffff'
    },
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      background: '#294159',
      outline: 'none',
      border: 'none',
      borderRadius: '10px'
    }
  };
  var customStyles = {
    table: {
      style: {
        backgroundColor: 'none'
      }
    },
    rows: {
      style: {
        backgroundColor: 'none',
        color: '#091D60'
      }
    },
    headRow: {
      style: {
        backgroundColor: 'rgba(84, 144, 255, 0.2)',
        borderRadius: '10px',
        color: '#091D60'
      }
    },
    pagination: {
      style: {
        backgroundColor: 'tranparent',
        color: 'rgba(0, 0, 0, 0.5);'
      },
      pageButtonsStyle: {
        fill: 'rgba(0, 0, 0, 0.5);',
        ':disabled': {
          fill: 'rgba(0, 0, 0, 0.2);'
        }
      }
    }
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_data_table_component__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .ZP, {
    columns: columns,
    data: data,
    customStyles: customStyles,
    pagination: true,
    paginationServer: true,
    paginationPerPage: paginationPerPage,
    paginationDefaultPage: meta === null || meta === void 0 ? void 0 : meta.page,
    paginationTotalRows: meta === null || meta === void 0 ? void 0 : meta.totalData,
    onChangePage: onChangePage,
    paginationComponentOptions: {
      noRowsPerPage: true,
      rangeSeparatorText: 'dari'
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement((react_modal__WEBPACK_IMPORTED_MODULE_2___default()), {
    isOpen: modalOpen,
    ariaHideApp: false,
    style: modalStyle
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-80 h-72 text-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "flex justify-end h-fit"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "image",
    src: _assets_icons_ic_cloce_svg__WEBPACK_IMPORTED_MODULE_4__,
    alt: "ic-close",
    onClick: function onClick() {
      setModalOpen(false);
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "grid justify-items-center text-white space-y-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    className: "font-semibold text-2xl"
  }, "Hapus Pegawai"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pt-3 space-y-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    className: "animate-pulse",
    src: _assets_icons_ic_delete_user_png__WEBPACK_IMPORTED_MODULE_5__,
    alt: "ic-delete-user"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    className: "font-semibold"
  }, "User"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-7 space-x-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "w-28 h-9 bg-secondary-1 text-white rounded-10",
    onClick: function onClick() {
      setModalOpen(false);
    }
  }, "Batal"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "w-28 h-9 bg-red text-white rounded-10",
    onClick: function onClick() {}
  }, "Hapus")))));
};
__signature__(TableComponent, "useCallback{loadingCell}\nuseState{[modalOpen, setModalOpen](false)}");
TableComponent.defaultProps = defaultProps;
var _default = TableComponent;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(defaultProps, "defaultProps", "D:\\Project\\Base_Code\\src\\features\\beranda\\table.tsx");
  reactHotLoader.register(TableComponent, "TableComponent", "D:\\Project\\Base_Code\\src\\features\\beranda\\table.tsx");
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\features\\beranda\\table.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 1797:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ Beranda)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7294);
/* harmony import */ var _features_beranda_summaryCard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5874);
/* harmony import */ var _features_beranda_listData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8823);
/* harmony import */ var _features_beranda_formInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7883);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};




var Beranda = function Beranda() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("section", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_beranda_summaryCard__WEBPACK_IMPORTED_MODULE_1__/* .Summary */ .E, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "md:flex md:justify-between md:space-x-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_beranda_listData__WEBPACK_IMPORTED_MODULE_2__/* .EmployeeList */ .l, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_features_beranda_formInput__WEBPACK_IMPORTED_MODULE_3__/* .FormInput */ .y, null)));
};

;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(Beranda, "Beranda", "D:\\Project\\Base_Code\\src\\pages\\beranda\\beranda.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 1199:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _beranda__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1797);
/* module decorator */ module = __webpack_require__.hmd(module);
(function () {
  var enterModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.enterModule : undefined;
  enterModule && enterModule(module);
})();
var __signature__ = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal["default"].signature : function (a) {
  return a;
};

var _default = _beranda__WEBPACK_IMPORTED_MODULE_0__/* .Beranda */ .T;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_default);
;
(function () {
  var reactHotLoader = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.default : undefined;
  if (!reactHotLoader) {
    return;
  }
  reactHotLoader.register(_default, "default", "D:\\Project\\Base_Code\\src\\pages\\beranda\\index.tsx");
})();
;
(function () {
  var leaveModule = typeof reactHotLoaderGlobal !== 'undefined' ? reactHotLoaderGlobal.leaveModule : undefined;
  leaveModule && leaveModule(module);
})();

/***/ }),

/***/ 6659:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
// extracted by mini-css-extract-plugin
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({"summaryCard":"retpAMkUhOFoMuOZDlXz","wrapperCard":"L9wmM8MjObSUguKJxHBe","wrapperGrafik":"EU_IYap9TSez9j58fdk2","table":"a7YyDfImyy1FEk4wr60C","header":"GeTp1Vl9yMZkDpScxfZk","row":"dpAPFpCnmR5Y6UpEBeah"});

/***/ }),

/***/ 2663:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/aac778b55f5e3d78332c.svg";

/***/ }),

/***/ 9371:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/9ed705f36d37bf65349e.png";

/***/ }),

/***/ 1874:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/a25c27f5c6b4a529c119.png";

/***/ }),

/***/ 3126:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/c1a95404c1c5bc4f5731.png";

/***/ }),

/***/ 9881:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/9917fc15a44733a5059a.png";

/***/ }),

/***/ 7658:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/0ca1f928231736d668d9.png";

/***/ }),

/***/ 3386:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__.p + "assets/images/2f2f98746402e8042aa9.png";

/***/ })

}]);
//# sourceMappingURL=199.0adc424c3dbbf46b5b33.bundle.js.map